package com.aait.spacek.Network

import com.aait.spacek.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface Service {
    @FormUrlEncoded
    @POST("sign-up")
    fun SignUp(@Field("type") type:String,
        @Field("name") name:String,
               @Field("phone") phone:String,
               @Field("email") email:String,
               @Field("password") password:String,
               @Field("device_id") device_id:String,
               @Field("device_type") device_type:String,
               @Header("lang") lang:String): Call<UserResponse>

    @FormUrlEncoded
    @POST("Check-Code")
    fun CheckCode(@Field("user_id") user_id:Int,
                  @Field("code") code:String,
                  @Header("lang") lang: String):Call<UserResponse>

    @FormUrlEncoded
    @POST("sign-in")
    fun Login(@Field("phone") phone:String,
              @Field("password") password:String,
              @Field("device_id") device_id:String,
              @Field("device_type") device_type:String,
              @Header("lang") lang: String):Call<UserResponse>

    @POST("get-profile-info")
    fun Edit(@Query("user_id") user_id: Int,
             @Header("lang") lang:String):Call<UserResponse>
    @FormUrlEncoded
    @POST("update-profile")
    fun Edit(@Field("user_id") user_id: Int,
             @Header("lang") lang:String,
             @Field("name") name:String?,
             @Field("phone") phone: String?,
    @Field("email") email:String?,
    @Field("lat") lat: String?,
    @Field("lng") lng: String?,
    @Field("address") address:String?):Call<UserResponse>
    @Multipart
    @POST("update-profile")
    fun AddImage(@Query("user_id") user_id: Int,
                 @Header("lang") lang:String,
                 @Part avatar: MultipartBody.Part,
                 @Query("lat") lat: String?,
                 @Query("lng") lng: String?,
                 @Query("address") address:String?):Call<UserResponse>
    @FormUrlEncoded
    @POST("change_password")
    fun resetPassword(@Header("lang") lang:String,
                      @Field("user_id") user_id: Int,
                      @Field("old_password") current_password:String,
                      @Field("new_password") password:String):Call<BaseResponse>


    @POST("about-us")
    fun About(@Header("lang") lang: String):Call<TermsResponse>

    @POST("site-policy")
    fun Terms(@Header("lang") lang: String):Call<TermsResponse>

    @POST("social-links")
    fun CallUs(@Header("lang") lang:String):Call<CallUsResponse>

    @POST("contact-us")
    fun Contact(@Header("lang") lang:String,
                @Query("name")  name:String,
                @Query("phone")  phone:String,
                @Query("email")  email:String,
                @Query("msg")  msg:String):Call<BaseResponse>

    @POST("filters")
    fun Filters(@Header("lang") lang: String):Call<FiltersResponse>
//
//    @POST("home")
//    fun getHome(@Header("lang") lang:String,
//    @Query("lat") lat:String,
//    @Query("lng") lng:String):Call<HomeResponse>
//
    @POST("view-provider")
    fun getProvider(@Header("lang") lang:String,
    @Query("user_id") user_id: Int?,
    @Query("provider_id") provider_id:Int,
    @Query("lat") lat: String,
    @Query("lng") lng: String,
    @Query("day_id") day_id:Int):Call<ProviderDetailsResponse>

    @POST("updateDeviceData")
    fun Notify(@Query("device_id") device_id:String,
               @Query("user_id") user_id:Int,
               @Query("notify") notify:Boolean,
               @Header("lang") lang:String):Call<BaseResponse>
//    @POST("view-provider-products")
//    fun Products(@Header("lang") lang:String,
//    @Query("provider_id") provider_id: Int):Call<ProductsResponse>
//
//    @POST("post-favourite")
//    fun AddFav(@Header("lang") lang: String,
//    @Query("user_id") user_id: Int,
//    @Query("provider_id") provider_id: Int):Call<BaseResponse>
//
//    @POST("my-favourites")
//    fun Favourites(@Query("user_id") user_id: Int,
//    @Header("lang") lang: String,
//    @Query("lat") lat: String,
//    @Query("lng") lng: String):Call<FavouritesResponse>
//

    @POST("reservation-details")
    fun Reservation(@Header("lang") lang: String,
                    @Query("reservation_id") reservation_id:Int):Call<ReservationDetailsResponse>
    @POST("main-search")
    fun Search(@Header("lang") lang: String,
    @Query("category_id") category_id:ArrayList<Int>?,
    @Query("lat") lat: String?,
    @Query("lng") lng: String?,
    @Query("name") name: String?,
    @Query("rate_price") rate_price:String?,
    @Query("number") rate:String?,
    @Query("price") is_open:String?,
    @Query("is_normal") occasion:Boolean?,
    @Query("sessions") sessions:String?,
    @Query("features") features:String?,
    @Query("addition_ids") addition_ids:ArrayList<Int>?,
    @Query("space_id") space_id:Int?,
    @Query("day_id") day_id: Int):Call<FilterResponse>

    @POST("logout")
    fun Logout(@Query("user_id") user_id: Int):Call<BaseResponse>

//    @POST("reservation-dividTime")
//    fun Times(@Header("lang") lang: String,
//        @Query("user_id") user_id: Int,
//        @Query("provider_id") provider_id: Int,
//        @Query("date") date:String):Call<TimesResponse>
//
    @POST("make-reservation")
    fun MakeReservation(@Header("lang") lang:String,
    @Query("user_id") user_id: Int,
    @Query("provider_id") provider_id: Int,
    @Query("date") start_date:String,
    @Query("time") start_time:String,
    @Query("count") count:String,
    @Query("lat") lat:String,
                        @Query("lng") lng: String,
                        @Query("day_id") day_id: Int,
                        @Query("hour") hour:String?
    ):Call<BaseResponse>

    @POST("my-reservations")
    fun Reservations(@Header("lang") lang:String,
    @Query("user_id") user_id: Int,
    @Query("lat") lat:String,
    @Query("lng") lng: String):Call<ReservationResponse>

    @POST("resend-code")
    fun Resend(@Header("lang") lang:String,
                @Query("user_id") user_id: Int):Call<UserResponse>

    @POST("forget-password")
    fun Forgot(@Header("lang") lang: String,
               @Query("phone") phone:String):Call<UserResponse>

    @POST("reset-password")
    fun NewPass(@Query("user_id") user_id: Int,
                @Query("password") password:String,
                @Query("code") code:String,
                @Header("lang") lang: String):Call<UserResponse>

    @POST("rate-provider")
    open fun Rate(
        @Header("lang") lang: String?,
        @Query("user_id") user_id: Int,
        @Query("provider_id") provider_id: Int,
        @Query("rate") rate: Int,
        @Query("rate_salary") rate_salary: Int,
        @Query("comment") comment: String?
    ): Call<BaseResponse>

    @POST("cancel-reservation")
    fun Cancel(@Header("lang") lang: String,
    @Query("reservation_id") reservation_id:Int):Call<BaseResponse>

    @POST("get-cities")
    fun Cities(@Header("lang") lang: String):Call<ListResponse>
    @POST("categories-list")
    fun Categories(@Header("lang") lang: String):Call<ListResponse>

    @POST("my-notifications")
    fun Notification(@Header("lang") lang: String,
    @Query("user_id") user_id: Int):Call<NotificationResponse>

}