package com.aait.spacek.UI.Activities.Main

import android.Manifest
import android.content.ClipData
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Gps.GPSTracker
import com.aait.spacek.Gps.GpsTrakerListener
import com.aait.spacek.Models.BaseResponse
import com.aait.spacek.Models.ImageModel
import com.aait.spacek.Models.ReservationDetailsResponse
import com.aait.spacek.Models.ReservationModel
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Controllers.SlidersAdapter
import com.aait.spacek.Utils.CommonUtil
import com.aait.spacek.Utils.DialogUtil
import com.aait.spacek.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class ReservationDetailsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_reservation_details
    lateinit var back:ImageView
    lateinit var title:TextView
//    lateinit var image:CircleImageView
//    lateinit var rating: RatingBar
//    lateinit var name:TextView
//    lateinit var type:TextView
//    lateinit var distance:TextView
//    lateinit var date:TextView
//    lateinit var time:TextView
//    lateinit var count:TextView
//    lateinit var map:MapView
    lateinit var pay:Button
    lateinit var name:TextView
    lateinit var image:ImageView
    lateinit var all:TextView
    lateinit var hour_lay:LinearLayout
    lateinit var hour_price:TextView
    lateinit var total:TextView
   // lateinit var type:TextView
    lateinit var distance:TextView
    lateinit var closed:TextView
    lateinit var address:TextView
    lateinit var rating:RatingBar
    lateinit var cancel:Button
    lateinit var date:TextView
    lateinit var re_time:TextView
    lateinit var num:TextView

    lateinit var reservationModel: ReservationModel
    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var favourite: LinearLayout
    lateinit var favourite_image: ImageView
    lateinit var favourite_text: TextView

    lateinit var my_reservations: LinearLayout

    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var more: LinearLayout

    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    override fun initializeComponents() {
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        favourite = findViewById(R.id.favourite)
        favourite_image = findViewById(R.id.favourite_image)
        favourite_text = findViewById(R.id.favourite_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)
        home.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","home")
            startActivity(intent)
            finish()}
        favourite.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","notification")
            startActivity(intent)
            finish()
        }
        my_reservations.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","reservation")
            startActivity(intent)
            finish()
        }
        more.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","more")
            startActivity(intent)
            finish()}
        reservationModel = intent.getSerializableExtra("data") as ReservationModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        rating = findViewById(R.id.rating)
        name = findViewById(R.id.name)
        //type = findViewById(R.id.category)
        distance = findViewById(R.id.time)
        address = findViewById(R.id.address)
        closed = findViewById(R.id.count)
        date = findViewById(R.id.date)
        re_time = findViewById(R.id.re_time)
        num = findViewById(R.id.num)
        pay = findViewById(R.id.pay)
        cancel = findViewById(R.id.cancel)
        total = findViewById(R.id.total)
        all = findViewById(R.id.all)
        hour_lay = findViewById(R.id.price_lay)
        hour_price = findViewById(R.id.hour_price)
        title.text = getString(R.string.reservation_details)
        back.setOnClickListener { onBackPressed()
        finish()}
//        map.onCreate(mSavedInstanceState)
//        map.onResume()
//        map.getMapAsync(this)
//
//        try {
//            MapsInitializer.initialize(mContext)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
        Glide.with(mContext).load(reservationModel.image).into(image)
        rating.rating = reservationModel?.provider?.get(0)?.rate?.toFloat()!!
        name.text = reservationModel.name
        address.text = reservationModel.address
        distance.text = reservationModel?.provider?.get(0)?.hour_price.toString()
        closed.text = reservationModel.total.toString()
        num.text = reservationModel.numbers.toString()
        re_time.text = reservationModel.time
        date.text = reservationModel.date
        hour_price.text = reservationModel.total+getString(R.string.riyal)
        total.text = reservationModel.total+getString(R.string.riyal)
        if (reservationModel.provider?.get(0)?.hour==null){
            hour_lay.visibility = View.GONE
            all.visibility = View.VISIBLE
        }else{
            hour_lay.visibility = View.VISIBLE
            all.visibility = View.GONE
        }
        if (reservationModel.is_paid.equals("true")){
            pay.visibility = View.GONE
        }else{
            //if accepted
            if (reservationModel.status.equals("completed")){
                pay.visibility = View.VISIBLE
            }else{
                pay.visibility = View.GONE
            }
        }
        if (reservationModel.status.equals("pending")||reservationModel.status.equals("accepted")){
            cancel.visibility = View.VISIBLE
        }else{
            cancel.visibility = View.GONE
        }
        pay.setOnClickListener {
            val intent = Intent(this,PayOnlineActivity::class.java)
            intent.putExtra("id",reservationModel.id)
            startActivity(intent)
            finish()
        }
         //getData()
        cancel.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.Cancel(lang.appLanguage,reservationModel.id!!)?.enqueue(object :
                Callback<BaseResponse>{
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            startActivity(Intent(this@ReservationDetailsActivity,MainActivity::class.java))
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
        }
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Reservation(lang.appLanguage,reservationModel.id!!)?.enqueue(object :Callback<ReservationDetailsResponse>{
            override fun onResponse(call: Call<ReservationDetailsResponse>, response: Response<ReservationDetailsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){


                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<ReservationDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

        })
    }




}