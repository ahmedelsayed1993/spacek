package com.aait.spacek.UI.Activities.Auth

import android.content.Intent
import android.text.InputType
import android.widget.*
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Models.UserModel
import com.aait.spacek.Models.UserResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Main.MainActivity
import com.aait.spacek.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewPassActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_new_pass

    lateinit var code: EditText
    lateinit var password: EditText
    lateinit var confirm_password: EditText
    lateinit var confirm: Button
    lateinit var view:ImageView
    lateinit var view1:ImageView
    lateinit var userModel: UserModel
    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var favourite: LinearLayout
    lateinit var favourite_image: ImageView
    lateinit var favourite_text: TextView

    lateinit var my_reservations: LinearLayout

    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var more: LinearLayout

    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    override fun initializeComponents() {
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        favourite = findViewById(R.id.favourite)
        favourite_image = findViewById(R.id.favourite_image)
        favourite_text = findViewById(R.id.favourite_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)
        home.setOnClickListener { val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("type","home")
            startActivity(intent)
            finish()}
        favourite.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("type","notification")
            startActivity(intent)
            finish()
        }
        my_reservations.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("type","reservation")
            startActivity(intent)
            finish()
        }
        more.setOnClickListener { val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("type","more")
            startActivity(intent)
            finish()}
        userModel = intent.getSerializableExtra("data") as UserModel
        code = findViewById(R.id.code)
        password = findViewById(R.id.password)
        confirm = findViewById(R.id.confirm)
        confirm_password = findViewById(R.id.confirm_pass)
        view = findViewById(R.id.view)
        view1 = findViewById(R.id.view1)
        view.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        view1.setOnClickListener { if (confirm_password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            confirm_password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            confirm_password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        confirm.setOnClickListener{
            if (CommonUtil.checkEditError(code,getString(R.string.verification_code))||
                CommonUtil.checkEditError(password,getString(R.string.new_pass))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_new_pass))){
                return@setOnClickListener
            }else{
                if (!password.text.toString().equals(confirm_password.text.toString())){
                    confirm_password.error = getString(R.string.password_not_match)
                }else{
                    NewPassword()
                }
            }
        }

    }

    fun NewPassword(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.NewPass(userModel.id!!,password.text.toString(),code.text.toString(),lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@NewPassActivity,LoginActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}