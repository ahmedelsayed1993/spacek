package com.aait.spacek.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.aait.spacek.Base.ParentRecyclerAdapter
import com.aait.spacek.Base.ParentRecyclerViewHolder

import com.aait.spacek.Models.SocialModel
import com.aait.spacek.R
import com.bumptech.glide.Glide

class SocialAdapter  (context: Context, data: MutableList<SocialModel>, layoutId: Int) :
    ParentRecyclerAdapter<SocialModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)

        Glide.with(mcontext).load(questionModel.logo).into(viewHolder.image)





        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_right)
        animation.setDuration(750)
        viewHolder.itemView.startAnimation(animation)

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var image = itemView.findViewById<ImageView>(R.id.icon)
    }
}