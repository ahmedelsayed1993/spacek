package com.aait.spacek.UI.Controllers

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.spacek.Base.ParentRecyclerAdapter
import com.aait.spacek.Base.ParentRecyclerViewHolder
import com.aait.spacek.Models.NotificationModel

import com.aait.spacek.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class NotificationAdapter (context: Context, data: MutableList<NotificationModel>, layoutId: Int) :
    ParentRecyclerAdapter<NotificationModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.msg)

       Glide.with(mcontext).asBitmap().load(questionModel.image).into(viewHolder.image)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_right)
        animation.setDuration(750)
        viewHolder.itemView.startAnimation(animation)

//        viewHolder.name.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)
//
//        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image=itemView.findViewById<CircleImageView>(R.id.image)




    }
}