package com.aait.spacek.UI.Activities.Auth

import android.content.Intent
import android.text.InputType
import android.util.Log
import android.widget.*
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Models.UserResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Appinfo.AppPolicyActivity
import com.aait.spacek.UI.Activities.Appinfo.TermsActivity
import com.aait.spacek.Utils.CommonUtil
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_register
    lateinit var user_name: EditText
    lateinit var phone: EditText
    lateinit var email:EditText
    lateinit var password: EditText
    lateinit var confirm_password: EditText
    lateinit var login: LinearLayout
    lateinit var register: Button
    lateinit var terms:CheckBox
    lateinit var view:ImageView
    lateinit var view1:ImageView
    var deviceID = ""
    override fun initializeComponents() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device",deviceID)
            // Log and toast

        })
        user_name = findViewById(R.id.user_name)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        confirm_password = findViewById(R.id.confirm_password)
        login = findViewById(R.id.login)
        register = findViewById(R.id.register)
        email = findViewById(R.id.email)
        terms = findViewById(R.id.terms)
        view = findViewById(R.id.view)
        view1 = findViewById(R.id.view1)
        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java)) }
        register.setOnClickListener {
            if(CommonUtil.checkEditError(user_name,getString(R.string.user_name))||
                CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_password))){
                return@setOnClickListener
            }else{
                if (!password.text.toString().equals(confirm_password.text.toString())) {
                    CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
                }else{
                    if (terms.isChecked) {
                        Register()
                    }else{
                        CommonUtil.makeToast(mContext,getString(R.string.terms))
                    }
                }
            }
        }
        view.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        view1.setOnClickListener { if (confirm_password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            confirm_password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            confirm_password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }

    }

    fun Register(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp("client",user_name.text.toString(),phone.text.toString(),email.text.toString()
            ,password.text.toString(),deviceID,"android",lang.appLanguage)?.enqueue(object:
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@RegisterActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}