package com.aait.spacek.UI.Activities.Main

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Gps.GPSTracker
import com.aait.spacek.Gps.GpsTrakerListener
import com.aait.spacek.Listeners.OnItemClickListener
import com.aait.spacek.Models.BaseResponse
import com.aait.spacek.Models.FilterModel
import com.aait.spacek.Models.ImageModel
import com.aait.spacek.Models.ProviderDetailsResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Auth.LoginActivity
import com.aait.spacek.UI.Controllers.FilterAdapter
import com.aait.spacek.UI.Controllers.SliderAdapter
import com.aait.spacek.UI.Controllers.SlidersAdapter
import com.aait.spacek.Utils.CommonUtil
import com.aait.spacek.Utils.DialogUtil
import com.aait.spacek.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderViewPager
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class ProductDetailsActivity:ParentActivity(), OnMapReadyCallback,
    GpsTrakerListener,OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_product_details
    lateinit var back:ImageView
    lateinit var viewPager: CardSliderViewPager
    lateinit var images:TextView
    lateinit var name:TextView
    lateinit var rating:RatingBar
    lateinit var type:TextView
    lateinit var description:TextView
    lateinit var fav:ImageView
    lateinit var share:ImageView
    lateinit var phone:ImageView
    lateinit var products:ImageView

    lateinit var rate:Button
    lateinit var types:TextView
    lateinit var additional:TextView
    lateinit var book:Button
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal  var myMarker: Marker?=null
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    lateinit var image:ImageView
    lateinit var activity_name:TextView
    var mLang = ""
    var mLat = ""
    var result = ""
    var id = 0
    var link = ""
    var num = ""
    var Lat=""
    var Lng = ""
    lateinit var map:MapView
    lateinit var go_now:Button
    var imagesModels = ArrayList<ImageModel>()
    var features = ArrayList<String>()
    lateinit var product:RecyclerView
    lateinit var linearLayoutManager: GridLayoutManager
    var day = 0
    lateinit var filterAdapter: FilterAdapter
    var providers = ArrayList<FilterModel>()
    lateinit var address:TextView
    lateinit var capacity:TextView
    lateinit var hours_price:TextView
    lateinit var lay:LinearLayout
    lateinit var hour_lay:LinearLayout
    lateinit var num_hours:EditText
    lateinit var confirm:Button
    lateinit var full_price:TextView
    var price = 0
    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var favourite: LinearLayout
    lateinit var favourite_image: ImageView
    lateinit var favourite_text: TextView

    lateinit var my_reservations: LinearLayout

    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var more: LinearLayout

    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    override fun initializeComponents() {
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        favourite = findViewById(R.id.favourite)
        favourite_image = findViewById(R.id.favourite_image)
        favourite_text = findViewById(R.id.favourite_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)
        home.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","home")
            startActivity(intent)
            finish()}
        favourite.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","notification")
            startActivity(intent)
            finish()
        }
        my_reservations.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","reservation")
            startActivity(intent)
            finish()
        }
        more.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","more")
            startActivity(intent)
            finish()}
        id = intent.getIntExtra("id",0)
        day = intent.getIntExtra("day",0)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        map = findViewById(R.id.map)
        go_now = findViewById(R.id.go_now)
        lay = findViewById(R.id.lay)
        hour_lay = findViewById(R.id.hour_lay)
        hours_price = findViewById(R.id.hours_price)
        num_hours = findViewById(R.id.num_hours)
        confirm = findViewById(R.id.confirm)
        full_price = findViewById(R.id.full_price)
        viewPager = findViewById(R.id.viewPager)
        images = findViewById(R.id.images)
        name = findViewById(R.id.name)
        rating = findViewById(R.id.rating)
        type = findViewById(R.id.type)
        capacity = findViewById(R.id.capacity)
        product = findViewById(R.id.product)
        description = findViewById(R.id.description)
         types = findViewById(R.id.types)
        additional = findViewById(R.id.additional)
        address = findViewById(R.id.address)
        book = findViewById(R.id.book)
        image = findViewById(R.id.image)
        activity_name = findViewById(R.id.activity_name)
        linearLayoutManager = GridLayoutManager(mContext,2)
        filterAdapter = FilterAdapter(mContext,providers,R.layout.recycle_filter)
        filterAdapter.setOnItemClickListener(this)
        product.layoutManager = linearLayoutManager
        product.adapter = filterAdapter
        book.setOnClickListener {
            if (user.loginStatus!!) {

                val intent = Intent(this,MakeResrvationActivity::class.java)
                intent.putExtra("id",id)
                startActivity(intent)
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        getLocationWithPermission()


        go_now.setOnClickListener { startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr=" + mLat + "," + mLang + "&daddr=" + Lat + "," + Lng)
            )) }


        images.setOnClickListener { if (imagesModels.size==0){
        }else{
            val intent = Intent(this,ImagesActivity::class.java)
            intent.putExtra("images",imagesModels)
            startActivity(intent)
        }
        }
        confirm.setOnClickListener {
            if (num_hours.text.trim().toString().equals("")){

            }else{
                hours_price.text = (price*num_hours.text.toString().toInt()).toString()
            }
        }

    }

    fun getData(user_id:Int?,lat:String,lng:String){
       // showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProvider(lang.appLanguage,user_id,id,lat,lng,day)?.enqueue(object :
            Callback<ProviderDetailsResponse>{
            override fun onFailure(call: Call<ProviderDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ProviderDetailsResponse>,
                response: Response<ProviderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.body()?.value.equals("1")){
                    initSliderAds(response.body()?.data?.images!!)
                    imagesModels.clear()
                    for (i in 0..response.body()?.data?.images!!.size-1){
                        imagesModels.add(response.body()?.data?.images!!.get(i))
                    }
                    address.text = response.body()?.data?.address
                    activity_name.text = response.body()?.data?.category
                    Glide.with(mContext).load(response.body()?.data?.category_icon).into(image)
                    filterAdapter.updateAll(response.body()?.data?.similar_providers!!)
                    type.text = response.body()?.data?.hour_price.toString()+getString(R.string.riyal)
                    capacity.text = response.body()?.data?.capacity.toString()

                    images.text = response.body()?.data?.images?.size.toString()+ " "+getString(R.string.show_all_photos)
                    full_price.text = getString(R.string.in_case_of)+response.body()?.data?.day_price.toString()+getString(R.string.riyal)
                    if (response.body()?.data?.hour_price==null){
                        lay.visibility = View.GONE
                        hour_lay.visibility = View.GONE
                    }else{
                        price = response.body()?.data?.hour_price!!
                        lay.visibility = View.VISIBLE
                        hour_lay.visibility = View.VISIBLE
                    }
                    name.text = response.body()?.data?.name
                    rating.rating = response.body()?.data?.rate!!
                   // link = response.body()?.data?.link_provider!!

                    num = response.body()?.data?.phone!!
                    Lat = response.body()?.data?.lat!!
                    Lng = response.body()?.data?.lng!!
                    putMapMarker(response.body()?.data?.lat!!.toDouble(),response.body()?.data?.lng!!.toDouble())

//                    if (response.body()?.data?.allow_reservation.equals("true")){
//                        book.visibility = View.VISIBLE
//                    }else{
//                        book.visibility = View.GONE
//                    }
                    description.text = response.body()?.data?.desc

                    if (response.body()?.data?.birthday.equals("true")){
                        features.add(getString(R.string.Birth_anniversary))
                    }
                    if (response.body()?.data?.business_meeting.equals("true")){
                        features.add(getString(R.string.business_meeting))
                    }
                    if (response.body()?.data?.indoor.equals("true")){
                        features.add(getString(R.string.in_door))
                    }
                    if (response.body()?.data?.kids.equals("true")){
                        features.add(getString(R.string.Allow_children))
                    }
                    if (response.body()?.data?.marriage_anniversary.equals("true")){
                        features.add(getString(R.string.marriage_anniversary))
                    }
                    if (response.body()?.data?.motivational_owners.equals("true")){
                        features.add(getString(R.string.Suitable_for_people_of_determination))
                    }
                    if (response.body()?.data?.music.equals("true")){
                        features.add(getString(R.string.Availability_of_music))
                    }
                    if (response.body()?.data?.outdoor.equals("true")){
                        features.add(getString(R.string.out_door))
                    }
                    if (response.body()?.data?.smoking.equals("true")){
                        features.add(getString(R.string.Allow_smoking))
                    }
                    if (response.body()?.data?.special_event.equals("true")){
                        features.add(getString(R.string.special_event))
                    }
                    if (response.body()?.data?.surface_view.equals("true")){
                        features.add(getString(R.string.Roof_view))
                    }
                    Log.e("fea",features.joinToString(separator = ",",postfix = "",prefix = ""))
                     additional.text =  response.body()?.data?.additions!!.joinToString(separator = ",",postfix = "",prefix = "")
                     types.text = response.body()?.data?.space_type!!.joinToString(separator = ",",postfix = "",prefix = "")
                }
                else{
                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                }
            }

        })
    }



    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!
        getLocationWithPermission()
        hideProgressDialog()
    }


    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun initSliderAds(list: java.util.ArrayList<ImageModel>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE
        }
        else{
            viewPager.visibility= View.VISIBLE
            viewPager.adapter= SlidersAdapter(mContext!!,list)
        }
    }
    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap?.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.locatio))
        )!!
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        }
        else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())
                if (user.loginStatus!!) {
                    getData(user.userData.id,gps.latitude.toString(), gps.longitude.toString())
                }else{
                    getData(null,gps.latitude.toString(), gps.longitude.toString())
                }

            }
        }
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,ProductDetailsActivity::class.java)
        intent.putExtra("id",providers.get(position).id)
        intent.putExtra("day",day)
        startActivity(intent)
    }
}