package com.aait.spacek.UI.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Main.MainActivity

class SplashActivity : ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_splash
    var isSplashFinishid = false
    override fun initializeComponents() {
        Handler().postDelayed({
            // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true

                    var intent = Intent(this@SplashActivity, MainActivity::class.java)
                    intent.putExtra("type","home")
                    startActivity(intent)
                    finish()

            }, 2100)
        }, 1800)
    }
}