package com.aait.spacek.UI.Activities.Main

import android.content.Intent
import android.os.Handler
import android.widget.TextView
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.R

class DoneActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_done

    override fun initializeComponents() {
        Handler().postDelayed({
            // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({


                val intent = Intent(this,MainActivity::class.java)
                intent.putExtra("type","home")
                startActivity(intent)
                finish()

            }, 2100)
        }, 1800)


    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }
}