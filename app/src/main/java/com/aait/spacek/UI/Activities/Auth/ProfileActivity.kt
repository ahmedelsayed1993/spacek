package com.aait.spacek.UI.Activities.Auth

import android.Manifest
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import android.provider.Settings
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Gps.GPSTracker
import com.aait.spacek.Gps.GpsTrakerListener
import com.aait.spacek.Models.BaseResponse
import com.aait.spacek.Models.UserResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Main.MainActivity
import com.aait.spacek.Utils.CommonUtil
import com.aait.spacek.Utils.DialogUtil
import com.aait.spacek.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.util.*
import javax.microedition.khronos.egl.EGLDisplay

class ProfileActivity :ParentActivity(), GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_my_data
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var name:EditText
    lateinit var phone:EditText
    lateinit var email:EditText
    lateinit var save:Button
    lateinit var image:CircleImageView
    lateinit var change_password:Button
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null

    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var favourite: LinearLayout
    lateinit var favourite_image: ImageView
    lateinit var favourite_text: TextView

    lateinit var my_reservations: LinearLayout

    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var more: LinearLayout

    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    override fun initializeComponents() {
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        favourite = findViewById(R.id.favourite)
        favourite_image = findViewById(R.id.favourite_image)
        favourite_text = findViewById(R.id.favourite_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)
        home.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","home")
            startActivity(intent)
            finish()}
        favourite.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","notification")
            startActivity(intent)
            finish()
        }
        my_reservations.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","reservation")
            startActivity(intent)
            finish()
        }
        more.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","more")
            startActivity(intent)
            finish()}
        getLocationWithPermission()
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        save = findViewById(R.id.confirm)
        image = findViewById(R.id.image)
        change_password = findViewById(R.id.change_password)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.profile)
        getData()
        save.setOnClickListener {  if(CommonUtil.checkEditError(name,getString(R.string.user_name))||
            CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
            CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))){
            return@setOnClickListener
        }else{
            Update()
        }
        }
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        change_password.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_change_password)
            val old_pass = dialog?.findViewById<EditText>(R.id.old_pass)
            val new_pass = dialog?.findViewById<EditText>(R.id.new_pass)
            val confirm_pass = dialog?.findViewById<EditText>(R.id.confirm_pass)

            val save = dialog?.findViewById<Button>(R.id.confirm)


            save?.setOnClickListener {
                if (CommonUtil.checkEditError(old_pass,getString(R.string.old_password))||
                    CommonUtil.checkEditError(new_pass,getString(R.string.new_password))||
                    CommonUtil.checkLength(new_pass,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm_pass,getString(R.string.confirm_password))){
                    return@setOnClickListener
                }else{
                    if (!new_pass.text.toString().equals(confirm_pass.text.toString())){
                        confirm_pass.error = getString(R.string.password_not_match)
                    }else{
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.resetPassword(lang.appLanguage,user.userData.id!!,old_pass.text.toString(),new_pass.text.toString())?.enqueue(
                            object :Callback<BaseResponse>{
                                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                                    CommonUtil.handleException(mContext,t)
                                    t.printStackTrace()
                                    hideProgressDialog()
                                }

                                override fun onResponse(
                                    call: Call<BaseResponse>,
                                    response: Response<BaseResponse>
                                ) {
                                    hideProgressDialog()
                                    if (response.isSuccessful){
                                        if (response.body()?.value.equals("1")){
                                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                            dialog?.dismiss()
                                        }else{
                                            CommonUtil.makeToast(mContext,response.body()?.msg!!)

                                        }
                                    }
                                }
                            }
                        )
                    }
                }

            }
            dialog?.show()
        }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Edit(user.userData.id!!,lang.appLanguage)
            ?.enqueue(object: Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            user.userData = response.body()?.data!!
                            Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                            name.setText( response.body()?.data?.name)
                           // user_name.text = response.body()?.data?.name
                            phone.setText(response.body()?.data?.phone)
                            email.setText(response.body()?.data?.email)

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }

    fun Update(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Edit(user.userData.id!!,lang.appLanguage,name.text.toString(),phone.text.toString(),email.text.toString(),mLat,mLang
        ,result)
            ?.enqueue(object: Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            user.userData = response.body()?.data!!
                             Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                            name.setText( response.body()?.data?.name)
                            // user_name.text = response.body()?.data?.name
                            phone.setText(response.body()?.data?.phone)
                            email.setText(response.body()?.data?.email)
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            startActivity(Intent(this@ProfileActivity, MainActivity::class.java))
                            finish()

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }

    fun upLoad(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.AddImage(user.userData.id!!,lang.appLanguage,filePart,mLat,mLang
                ,result)?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        user.userData = response.body()?.data!!
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        name.setText( response.body()?.data?.name)
                        // user_name.text = response.body()?.data?.name
                        phone.setText(response.body()?.data?.phone)
                        email.setText(response.body()?.data?.email)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                Glide.with(mContext).load(ImageBasePath).into(image)

                if (ImageBasePath != null) {
                    upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext!!,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        }
        else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }




            }
        }
    }

}