package com.aait.spacek.UI.Activities.Main

import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Models.ImageModel
import com.aait.spacek.R
import com.aait.spacek.UI.Controllers.ImagesAdapter

class ImagesActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_images
     lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var images:RecyclerView
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var imagesAdapter: ImagesAdapter
    var imagesModels = ArrayList<ImageModel>()
    override fun initializeComponents() {
        imagesModels = intent.getSerializableExtra("images") as ArrayList<ImageModel>
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        images = findViewById(R.id.images)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = ""
        gridLayoutManager = GridLayoutManager(mContext,4)
        imagesAdapter = ImagesAdapter(mContext,ArrayList<ImageModel>(),R.layout.recycle_images)
        images.layoutManager = gridLayoutManager
        images.adapter = imagesAdapter
        imagesAdapter.updateAll(imagesModels)

    }
}