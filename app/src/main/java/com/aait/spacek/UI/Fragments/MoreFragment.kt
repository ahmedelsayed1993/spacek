package com.aait.spacek.UI.Fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.AppCompatDelegate
import com.aait.spacek.Base.BaseFragment
import com.aait.spacek.Models.BaseResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Appinfo.AboutAppActivity
import com.aait.spacek.UI.Activities.Appinfo.AppPolicyActivity
import com.aait.spacek.UI.Activities.Appinfo.CallUsActivity
import com.aait.spacek.UI.Activities.Appinfo.LangActivity
import com.aait.spacek.UI.Activities.Auth.LoginActivity
import com.aait.spacek.UI.Activities.Auth.ProfileActivity
import com.aait.spacek.UI.Activities.SplashActivity
import com.aait.spacek.Utils.CommonUtil
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoreFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_more
    companion object {
        fun newInstance(): MoreFragment {
            val args = Bundle()
            val fragment = MoreFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var notifi:Switch
     lateinit var title:TextView
    lateinit var profile: LinearLayout
    lateinit var share_app:LinearLayout
    lateinit var language: LinearLayout
    lateinit var call_us: LinearLayout
    lateinit var policy: LinearLayout
    lateinit var about_app: LinearLayout
    lateinit var logout: LinearLayout
    lateinit var text:TextView
    var deviceID = ""
    lateinit var show_location:LinearLayout
    override fun initializeComponents(view: View) {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device",deviceID)
            // Log and toast

        })
        show_location = view.findViewById(R.id.show_location)
        title = view.findViewById(R.id.title)
        title.text = getString(R.string.more)
        notifi = view.findViewById(R.id.notifi)
        text = view.findViewById(R.id.text)
        share_app = view.findViewById(R.id.share_app)
        profile = view.findViewById(R.id.profile)
        language = view.findViewById(R.id.language)
        call_us = view.findViewById(R.id.contact_us)
        policy = view.findViewById(R.id.terms)
        about_app = view.findViewById(R.id.about_app)
        logout = view.findViewById(R.id.logout)
        if (user.loginStatus!!){
            if (user.notificationStatus!!){
                notifi.isChecked = true
            }else{
                notifi.isChecked = false
            }
        }
        show_location.setOnClickListener { val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse("https://spacek.4hoste.com/store_login")
            startActivity(i) }
        notifi.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position
            if (user.loginStatus!!) {
                if (isChecked) {
                    // theme.appTheme =
                    Client.getClient()?.create(Service::class.java)?.Notify(deviceID, user.userData.id!!, true, lang.appLanguage)
                            ?.enqueue(object : Callback<BaseResponse> {
                                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body()?.value.equals("1")) {
                                            user.notificationStatus = true
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                                }

                            })

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
                } else {
                    //  theme.appTheme =
                    Client.getClient()?.create(Service::class.java)?.Notify(deviceID, user.userData.id!!, false, lang.appLanguage)
                            ?.enqueue(object : Callback<BaseResponse> {
                                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                                    if (response.isSuccessful) {
                                        if (response.body()?.value.equals("1")) {
                                            user.notificationStatus = false
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                                }

                            })

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
                }
            }
        })

        share_app.setOnClickListener { CommonUtil.ShareApp(mContext!!) }
        if (user.loginStatus!!) {
            text.text = getString(R.string.logout)
        }else {
            text.text = getString(R.string.sign_in)
        }
        profile.setOnClickListener {
            if (user.loginStatus!!) {
                startActivity(Intent(activity, ProfileActivity::class.java))
            }else{
                startActivity(Intent(activity,LoginActivity::class.java))
            }
        }

        about_app.setOnClickListener { startActivity(Intent(activity, AboutAppActivity::class.java)) }
        policy.setOnClickListener { startActivity(Intent(activity, AppPolicyActivity::class.java)) }
        call_us.setOnClickListener { startActivity(Intent(activity, CallUsActivity::class.java))  }
        language.setOnClickListener { startActivity(Intent(activity, LangActivity::class.java)) }
        logout.setOnClickListener {
            if (user.loginStatus!!){
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Logout(user.userData.id!!)?.enqueue(object :
                        Callback<BaseResponse> {
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                            call: Call<BaseResponse>,
                            response: Response<BaseResponse>
                    ) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                user.loginStatus=false
                                user.Logout()
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                                startActivity(Intent(activity, SplashActivity::class.java))
                                activity?.finish()
                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }

                })
            }else{
                startActivity(Intent(activity, LoginActivity::class.java))
                requireActivity().finish()
            }
        }


    }
    }
