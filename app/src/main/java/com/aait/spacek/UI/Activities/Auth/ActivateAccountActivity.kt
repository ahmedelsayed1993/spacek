package com.aait.spacek.UI.Activities.Auth

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Models.UserModel
import com.aait.spacek.Models.UserResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivateAccountActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_activation_code
    lateinit var code: EditText
    lateinit var confirm: Button
    lateinit var resend: TextView
    lateinit var userModel: UserModel

    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        code = findViewById(R.id.code)
        confirm = findViewById(R.id.confirm)
        resend = findViewById(R.id.resend)
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(code,getString(R.string.activation_code))){
                return@setOnClickListener
            }else{
                Activate()
            }
        }
        resend.setOnClickListener {
            Resend()
        }



    }

    fun Activate(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CheckCode(userModel.id!!,code.text.toString(),lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@ActivateAccountActivity, LoginActivity::class.java))
                        this@ActivateAccountActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun Resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Resend(lang.appLanguage,userModel.id!!)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<UserResponse>,
                response: Response<UserResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

}