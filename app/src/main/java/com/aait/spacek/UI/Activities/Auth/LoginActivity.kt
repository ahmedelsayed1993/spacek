package com.aait.spacek.UI.Activities.Auth

import android.content.Intent
import android.text.InputType
import android.util.Log
import android.widget.*
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Models.UserResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Main.MainActivity
import com.aait.spacek.Utils.CommonUtil
import com.google.android.gms.tasks.OnCompleteListener

import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_login
    lateinit var register: LinearLayout
    lateinit var phone: EditText
    lateinit var password: EditText
    lateinit var login: Button
    lateinit var forgot: TextView
    lateinit var visitor: TextView
    lateinit var view:ImageView
    var deviceID = ""
    override fun initializeComponents() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device",deviceID)
            // Log and toast

        })
        register = findViewById(R.id.register)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        login = findViewById(R.id.login)
        forgot = findViewById(R.id.forgot)
        visitor = findViewById(R.id.visitor)
        view = findViewById(R.id.view)
        view.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        visitor.setOnClickListener { user.loginStatus = false
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("type","home")
            startActivity(intent)
        }
        register.setOnClickListener { startActivity(Intent(this,RegisterActivity::class.java)) }
        forgot.setOnClickListener { startActivity(Intent(this,ForgotPassActivity::class.java)) }

        login.setOnClickListener {
            if(CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkEditError(password,getString(R.string.password))){
                return@setOnClickListener
            }else{
                Login()
            }
        }
    }

    fun Login(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Login(phone.text.toString()
            ,password.text.toString(),deviceID,"android",lang.appLanguage)?.enqueue(object:
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
                Log.e("error", Gson().toJson(t))
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")) {
                        if (response.body()?.data?.active.equals("active")) {
                            user.loginStatus = true
                            user.userData = response.body()?.data!!
                            val intent = Intent(this@LoginActivity, MainActivity::class.java)
                            intent.putExtra("type","home")
                            startActivity(intent)
                            finish()
                        } else if (response.body()?.data?.active.equals("pending")) {
                            val intent =
                                Intent(this@LoginActivity, ActivateAccountActivity::class.java)
                            intent.putExtra("user", response.body()?.data)
                            startActivity(intent)
                            finish()
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}