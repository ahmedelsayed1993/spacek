package com.aait.spacek.UI.Activities.Appinfo

import android.content.Intent
import android.view.View
import android.widget.*
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Main.MainActivity
import com.aait.spacek.UI.Activities.SplashActivity

class LangActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_lang
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var lang1:TextView
    lateinit var lay:RadioGroup
    lateinit var arabic:RadioButton
    lateinit var english:RadioButton

    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var favourite: LinearLayout
    lateinit var favourite_image: ImageView
    lateinit var favourite_text: TextView

    lateinit var my_reservations: LinearLayout

    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var more: LinearLayout

    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    override fun initializeComponents() {
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        favourite = findViewById(R.id.favourite)
        favourite_image = findViewById(R.id.favourite_image)
        favourite_text = findViewById(R.id.favourite_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)
        home.setOnClickListener { val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("type","home")
            startActivity(intent)
            finish()}
        favourite.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("type","notification")
            startActivity(intent)
            finish()
        }
        my_reservations.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("type","reservation")
            startActivity(intent)
            finish()
        }
        more.setOnClickListener { val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("type","more")
            startActivity(intent)
            finish()}
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        lang1 = findViewById(R.id.lang)
        lay = findViewById(R.id.lay)
        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.language)
        if (lang.appLanguage == "ar"){
            lang1.text = getString(R.string.arabic)
            arabic.isChecked = true
        }else{
            lang1.text = getString(R.string.english)
            english.isChecked = true
        }
        arabic.setOnClickListener {
            lang.appLanguage = "ar"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }
        english.setOnClickListener {
            lang.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }
        lang1.setOnClickListener { lay.visibility = View.VISIBLE }
    }
}