package com.aait.spacek.UI.Activities.Main

import android.Manifest
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Gps.GPSTracker
import com.aait.spacek.Gps.GpsTrakerListener
import com.aait.spacek.Models.BaseResponse
import com.aait.spacek.Models.ProviderDetailsResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Auth.LoginActivity
import com.aait.spacek.Utils.CommonUtil
import com.aait.spacek.Utils.DialogUtil
import com.aait.spacek.Utils.PermissionUtils
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class MakeResrvationActivity:ParentActivity(), GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_reserve
     lateinit var back:ImageView
     lateinit var title:TextView
     lateinit var st_date:TextView
     lateinit var ed_date:TextView
     lateinit var s_time:TextView
     lateinit var d_time:TextView
     lateinit var num:EditText
     lateinit var book:Button
     lateinit var reserve_all:RadioButton
     lateinit var hour_lay:LinearLayout
     lateinit var num_hours:EditText
     lateinit var hours_price:TextView
     lateinit var full_price:TextView
     lateinit var total_lay:LinearLayout
     lateinit var total:TextView
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
     var id = 0
    var mYear = 0
    var mMonth = 0
    var mDay = 0
    var price = 0
    var day_count = 0
    private var mAlertDialog: AlertDialog? = null
    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var favourite: LinearLayout
    lateinit var favourite_image: ImageView
    lateinit var favourite_text: TextView

    lateinit var my_reservations: LinearLayout

    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var more: LinearLayout

    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        favourite = findViewById(R.id.favourite)
        favourite_image = findViewById(R.id.favourite_image)
        favourite_text = findViewById(R.id.favourite_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)
        home.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
        intent.putExtra("type","home")
        startActivity(intent)
        finish()}
        favourite.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","notification")
            startActivity(intent)
            finish()
        }
        my_reservations.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","reservation")
            startActivity(intent)
            finish()
        }
        more.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","more")
            startActivity(intent)
            finish()}
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy")
        val formatter1 = DateTimeFormatter.ofPattern("EEEE , dd  MMMM")
        val formatter2 = DateTimeFormatter.ofPattern("yyyy/MM/dd")
        var answer: String =  current.format(formatter2)

        var day = LocalDateTime.now().dayOfWeek.toString()
        if (day.contains("MON")){
            day_count = 2
        }else if (day.contains("SUN")){
            day_count = 1
        }
        else if (day.contains("SAT")){
            day_count = 7
        }else if (day.contains("FRI")){
            day_count = 6
        }else if (day.contains("TUE")){
            day_count = 3
        }
        else if (day.contains("WED")){
            day_count = 4
        }else if (day.contains("THR")){
            day_count = 5
        }
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        st_date = findViewById(R.id.s_date)
        s_time = findViewById(R.id.s_time)
        reserve_all = findViewById(R.id.reserve_all)
        hour_lay = findViewById(R.id.hour_lay)
        num_hours = findViewById(R.id.num_hours)
        hours_price = findViewById(R.id.hours_price)
        full_price = findViewById(R.id.full_price)
        total = findViewById(R.id.total)
        total_lay = findViewById(R.id.total_lay)
        num = findViewById(R.id.num)
        book = findViewById(R.id.book)
        title.text = getString(R.string.reservation_data)
        st_date.text = answer
        back.setOnClickListener { onBackPressed()
        finish()}
        getLocationWithPermission()
        book.setOnClickListener {
            if (CommonUtil.checkTextError(st_date, getString(R.string.choose_arrival_date)) ||
                    CommonUtil.checkTextError(s_time, getString(R.string.choose_time)) ||
                    CommonUtil.checkEditError(num, getString(R.string.enter_num_people))) {
                return@setOnClickListener
            } else {
                if (reserve_all.isChecked) {
                    Reserve(null)
                }else{
                    if (CommonUtil.checkEditError(num_hours,getString(R.string.enter_num_of_hours))){
                        return@setOnClickListener
                    }else {
                        Reserve(num_hours.text.toString())
                    }
                }
            }
        }

         num_hours.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                }
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (num_hours.text.trim().toString().equals("")){
                        hours_price.text = ""
                    }else{
                        hours_price.text = (price*num_hours.text.toString().toInt()).toString()+getString(R.string.riyal)
                    }
                }
         })
          reserve_all.setOnClickListener {

              if (total_lay.visibility == View.GONE) {
                        total_lay.visibility = View.VISIBLE
                        reserve_all.isChecked = true
              } else {
                        total_lay.visibility = View.GONE
                        reserve_all.isChecked = false
              }

          }

            st_date.setOnClickListener(View.OnClickListener {
               // Log.e("ttt","tttt")
                val c = Calendar.getInstance()
                val year = c.get(Calendar.YEAR)
                val month = c.get(Calendar.MONTH)
                val day = c.get(Calendar.DAY_OF_MONTH)
                val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    var m = monthOfYear+1
                    st_date.setText("" + year + "/" + m + "/" + dayOfMonth)
                    day_count = c.get(Calendar.DAY_OF_WEEK)
                    getLocationWithPermission()
                }, year, month, day)
                dpd.datePicker.minDate = System.currentTimeMillis() - 1000
                dpd.show()
            })


        s_time.setOnClickListener {  val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)
            val myTimeListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        if (view.isShown) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                            myCalender.set(Calendar.MINUTE, minute)
                            var hour = hourOfDay
                            var minute = minute
                            // hour1 = hourOfDay.toString()
                            var am_pm = ""
                            val hours = if (hour < 10) "0" + hour else hour
                            val minutes = if (minute < 10) "0" + minute else minute
                            s_time.text = hours.toString()+":"+minutes.toString()
                        }
                    }
            val timePickerDialog = TimePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    myTimeListener,
                    hour,
                    minute,
                    false
            )
            timePickerDialog.setTitle(getString(R.string.start_time))
            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorWhite)
            timePickerDialog.show() }


    }

    fun Reserve(hour:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.MakeReservation(lang.appLanguage,user.userData.id!!,id,st_date.text.toString(),s_time.text.toString(),num.text.toString()
        ,"0.0","0.0",day_count,hour
                )
                ?.enqueue(object : Callback<BaseResponse> {
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(
                            call: Call<BaseResponse>,
                            response: Response<BaseResponse>
                    ) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                startActivity(Intent(this@MakeResrvationActivity,DoneActivity::class.java))
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    fun getData(lat:String,lng:String){
        // showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProvider(lang.appLanguage,null,id,lat,lng,day_count)?.enqueue(object :
            Callback<ProviderDetailsResponse>{
            override fun onFailure(call: Call<ProviderDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ProviderDetailsResponse>,
                response: Response<ProviderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.body()?.value.equals("1")){


                    full_price.text = getString(R.string.in_case_of)+response.body()?.data?.day_price.toString()+getString(R.string.riyal)
                    if (response.body()?.data?.hour_price==null){

                        hour_lay.visibility = View.GONE
                    }else{
                        price = response.body()?.data?.hour_price!!
                        total.text = response.body()?.data?.day_price.toString()+getString(R.string.riyal)
                        hour_lay.visibility = View.VISIBLE
                    }

                     }
                else{
                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                }
            }

        })
    }
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        }
        else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())
                getData(gps.latitude.toString(),gps.longitude.toString())

            }
        }
    }
}