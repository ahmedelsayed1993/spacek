package com.aait.spacek.UI.Activities.Main

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Gps.GPSTracker
import com.aait.spacek.Gps.GpsTrakerListener
import com.aait.spacek.Listeners.OnItemClickListener
import com.aait.spacek.Models.*
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Controllers.FilterAdapter
import com.aait.spacek.UI.Controllers.ListAdapter
import com.aait.spacek.UI.Controllers.SpaceAdapter
import com.aait.spacek.Utils.CommonUtil
import com.aait.spacek.Utils.DialogUtil
import com.aait.spacek.Utils.PermissionUtils
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class SearchActivity:ParentActivity()  , OnItemClickListener, GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_search
    lateinit var filter:ImageView
    lateinit var map:ImageView
    lateinit var lay:LinearLayout
    lateinit var cancel:ImageView
    lateinit var available:CheckBox
    lateinit var unavailable:CheckBox
    lateinit var birthday:RadioButton
    lateinit var marriage:RadioButton
    lateinit var business:RadioButton
    lateinit var event:RadioButton
    lateinit var cheap:RadioButton
    lateinit var avarage:RadioButton
     lateinit var space_type:RecyclerView
     lateinit var additions:RecyclerView
    lateinit var outdoor:RadioButton
    lateinit var indoor:RadioButton
    lateinit var roof:RadioButton
    lateinit var top:RadioButton
    lateinit var low:RadioButton
    lateinit var music:CheckBox
    lateinit var children:CheckBox
    lateinit var smoking:CheckBox
    lateinit var people:CheckBox
    lateinit var filter_btn:Button
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var search:EditText
    lateinit var sear:ImageView
    var sort :String?=null
    var ocassion :String?=null
    var prices :Boolean?=false
    var session :String?=null
    var rate:String?=null
    var more = ArrayList<String>()
    var cat = ArrayList<Int>()
    var city = ArrayList<Int>()
    lateinit var linearLayoutManager: GridLayoutManager
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var filterAdapter:FilterAdapter
    var providers = ArrayList<FilterModel>()
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    lateinit var listAdapter:ListAdapter
    var listModels = ArrayList<ListModel>()
    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    var shops = ArrayList<FilterModel>()
    private var mAlertDialog: AlertDialog? = null
    lateinit var num:EditText
    lateinit var price:EditText
    var ids = ArrayList<Int>()
    lateinit var spaceAdapter:SpaceAdapter
    var space = 0
    var spaces = ArrayList<ListModel>()
    var day = 0
    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var favourite: LinearLayout
    lateinit var favourite_image: ImageView
    lateinit var favourite_text: TextView

    lateinit var my_reservations: LinearLayout

    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var more_: LinearLayout

    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    override fun initializeComponents() {
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        favourite = findViewById(R.id.favourite)
        favourite_image = findViewById(R.id.favourite_image)
        favourite_text = findViewById(R.id.favourite_text)
        more_ = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)
        home.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","home")
            startActivity(intent)
            finish()}
        favourite.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","notification")
            startActivity(intent)
            finish()
        }
        my_reservations.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","reservation")
            startActivity(intent)
            finish()
        }
        more_.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","more")
            startActivity(intent)
            finish()}
        cat = intent.getIntegerArrayListExtra("cat") as ArrayList<Int>
        city = intent.getIntegerArrayListExtra("city") as ArrayList<Int>
        day = intent.getIntExtra("day",0)
        filter = findViewById(R.id.filter)
        map = findViewById(R.id.map)
        lay = findViewById(R.id.lay)
        cancel = findViewById(R.id.close)
        num = findViewById(R.id.num)
        price = findViewById(R.id.price)
        space_type = findViewById(R.id.space_type)
        additions = findViewById(R.id.additions)
        gridLayoutManager = GridLayoutManager(mContext,2)
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter = ListAdapter(mContext,listModels,R.layout.recycler_city)
        listAdapter.setOnItemClickListener(this)
        additions.layoutManager = linearLayoutManager1
        additions.adapter = listAdapter
        spaceAdapter = SpaceAdapter(mContext,spaces,R.layout.recycler_space)
        spaceAdapter.setOnItemClickListener(this)
        space_type.layoutManager = gridLayoutManager
        space_type.adapter = spaceAdapter
//        available = findViewById(R.id.available)
//        unavailable = findViewById(R.id.unavailable)
//        birthday = findViewById(R.id.birthday)
//        marriage = findViewById(R.id.marriage)
//        business = findViewById(R.id.business)
//        event = findViewById(R.id.event)
        cheap = findViewById(R.id.paid)
        avarage = findViewById(R.id.normal)
//        expensive = findViewById(R.id.expensive)
        outdoor = findViewById(R.id.outdoor)
        indoor = findViewById(R.id.indoor)
        roof = findViewById(R.id.roof)
//        top = findViewById(R.id.top)
//        low = findViewById(R.id.low)

        search = findViewById(R.id.search)
        sear = findViewById(R.id.sear)
        music = findViewById(R.id.music)
        children = findViewById(R.id.children)
        smoking = findViewById(R.id.smoking)
        people = findViewById(R.id.people)
        filter_btn = findViewById(R.id.confirm)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        back.setOnClickListener { onBackPressed()
        finish()}
        linearLayoutManager = GridLayoutManager(mContext,2)
        filterAdapter = FilterAdapter(mContext,providers,R.layout.recycle_filter)
        filterAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = filterAdapter
        lay.visibility = View.GONE
        filter.setOnClickListener { lay.visibility = View.VISIBLE }
        cancel.setOnClickListener { lay.visibility = View.GONE }
        Filter()
        if (lang.appLanguage.equals("ar")){
            cancel.setImageResource(R.mipmap.back)
        }else{
            cancel.setImageResource(R.mipmap.bac)
        }
        cheap.setOnClickListener {

                cheap.isChecked = true
                avarage.isChecked = false

                prices = false

        }
        avarage.setOnClickListener {

                cheap.isChecked = false
                avarage.isChecked = true

                prices = true

        }

        outdoor.setOnClickListener {

                outdoor.isChecked = true
                indoor.isChecked = false
                roof.isChecked = false
                session = "outdoor"

        }
        indoor.setOnClickListener {

                outdoor.isChecked = false
                indoor.isChecked = true
                roof.isChecked = false
                session = "indoor"

        }
        roof.setOnClickListener {

                outdoor.isChecked = false
                indoor.isChecked = false
                roof.isChecked = true
                session = "surface_view"

        }

        music.setOnClickListener {

                if (!more.contains("music")){

                        more.add("music")


                }

        }
        children.setOnClickListener {


                if (!more.contains("kids")){
                    more.add("kids")
                }

        }
        smoking.setOnClickListener {


                if (!more.contains("smoking")){
                    more.add("smoking")
                }

        }
        people.setOnClickListener {

                if (!more.contains("motivational_owners")){
                    more.add("motivational_owners")
                }

        }

        filter_btn.setOnClickListener {

            Log.e("more",Gson().toJson(more))
            lay.visibility = View.GONE
            getLocationWithPermission()
        }
        sear.setOnClickListener { getLocationWithPermission() }
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getLocationWithPermission()

        }
        getLocationWithPermission()
        map.setOnClickListener {
            val intent = Intent(this,MapActivity::class.java)
            intent.putExtra("day",day)
            startActivity(intent)
        }
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            listAdapter.notifyDataSetChanged()
            if (listModels.get(position).checked!!){
                ids.add(listModels.get(position).id!!)

            }else{
                ids.remove(listModels.get(position).id!!)

            }
        }else if (view.id == R.id.space){
            spaceAdapter.selected = position
            spaces.get(position).checked = true
            spaceAdapter.notifyDataSetChanged()
            space = spaces.get(position).id!!
            Log.e("reason",space.toString())
        } else{
            val intent = Intent(this, ProductDetailsActivity::class.java)
            intent.putExtra("id", providers.get(position).id)
            intent.putExtra("day",day)
            startActivity(intent)
        }
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        }
        else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())
                if (more.isEmpty()){
                    if (space==0) {
                        getData(gps.latitude.toString(), gps.longitude.toString(), null, null)
                    }else{
                        getData(gps.latitude.toString(), gps.longitude.toString(), null, space)

                    }
                }else{
                    if (space==0) {
                        getData(
                            gps.latitude.toString(),
                            gps.longitude.toString(),
                            Gson().toJson(more),
                            null
                        )
                    }else{
                        getData(
                            gps.latitude.toString(),
                            gps.longitude.toString(),
                            Gson().toJson(more),
                            space
                        )
                    }
                }
               // getData(gps.latitude.toString(),gps.longitude.toString())

            }
        }
    }
    fun getData(lat:String,lng:String,features:String?,space_:Int?){
        // showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        if (cat.isEmpty()) {
            if (ids.isEmpty()){
            Client.getClient()?.create(Service::class.java)?.Search(
                    lang.appLanguage,
                    null,
                    lat,
                    lng,
                    search.text.toString(),
                    null,
                    num.text.toString() ,
                    price.text.toString(),
                    prices,
                    session,
                    features,null
            ,space_,day)?.enqueue(object :
                Callback<FilterResponse> {
                override fun onFailure(call: Call<FilterResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    layNoInternet!!.visibility = View.VISIBLE
                    layNoItem!!.visibility = View.GONE
                    swipeRefresh!!.isRefreshing = false
                }

                override fun onResponse(
                    call: Call<FilterResponse>,
                    response: Response<FilterResponse>
                ) {
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                    more.clear()
                    shops.clear()
                    if (response.body()?.value.equals("1")) {
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
                            filterAdapter.updateAll(response.body()?.data!!)
                            for (i in 0..response.body()?.data?.size!!-1) {
                                shops.add( response.body()?.data?.get(i)!!)
                            }
                        }

                    } else {
                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                    }
                }

            })}else{
                Client.getClient()?.create(Service::class.java)?.Search(
                    lang.appLanguage,
                    null,
                    lat,
                    lng,
                    search.text.toString(),
                    null,
                    num.text.toString() ,
                    price.text.toString(),
                    prices,
                    session,
                    features,ids
                    ,space_ ,day)?.enqueue(object :
                    Callback<FilterResponse> {
                    override fun onFailure(call: Call<FilterResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext, t)
                        t.printStackTrace()
                        layNoInternet!!.visibility = View.VISIBLE
                        layNoItem!!.visibility = View.GONE
                        swipeRefresh!!.isRefreshing = false
                    }

                    override fun onResponse(
                        call: Call<FilterResponse>,
                        response: Response<FilterResponse>
                    ) {
                        hideProgressDialog()
                        swipeRefresh!!.isRefreshing = false
                        more.clear()
                        shops.clear()
                        if (response.body()?.value.equals("1")) {
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                            } else {
                                filterAdapter.updateAll(response.body()?.data!!)
                                for (i in 0..response.body()?.data?.size!!-1) {
                                    shops.add( response.body()?.data?.get(i)!!)
                                }
                            }

                        } else {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                        }
                    }

                })
            }
        }else{
            if (ids.isEmpty()){
            Client.getClient()?.create(Service::class.java)?.Search(
                lang.appLanguage, cat,
                lat,
                lng,
                search.text.toString(),
                null,
                    num.text.toString() ,
                    price.text.toString(),
                prices,
                session,
                features,null
                ,space_ ,day)?.enqueue(object :
                Callback<FilterResponse> {
                override fun onFailure(call: Call<FilterResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    layNoInternet!!.visibility = View.VISIBLE
                    layNoItem!!.visibility = View.GONE
                    swipeRefresh!!.isRefreshing = false
                }

                override fun onResponse(
                    call: Call<FilterResponse>,
                    response: Response<FilterResponse>
                ) {
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                    more.clear()
                    if (response.body()?.value.equals("1")) {
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
                            filterAdapter.updateAll(response.body()?.data!!)
                            for (i in 0..response.body()?.data?.size!!-1) {
                                shops.add( response.body()?.data?.get(i)!!)
                            }
                        }

                    } else {
                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                    }
                }

            })}else{
                Client.getClient()?.create(Service::class.java)?.Search(
                    lang.appLanguage, cat,
                    lat,
                    lng,
                    search.text.toString(),
                    null,
                    num.text.toString() ,
                    price.text.toString(),
                    prices,
                    session,
                    features,ids
                    ,space_,day)?.enqueue(object :
                    Callback<FilterResponse> {
                    override fun onFailure(call: Call<FilterResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext, t)
                        t.printStackTrace()
                        layNoInternet!!.visibility = View.VISIBLE
                        layNoItem!!.visibility = View.GONE
                        swipeRefresh!!.isRefreshing = false
                    }

                    override fun onResponse(
                        call: Call<FilterResponse>,
                        response: Response<FilterResponse>
                    ) {
                        hideProgressDialog()
                        swipeRefresh!!.isRefreshing = false
                        more.clear()
                        if (response.body()?.value.equals("1")) {
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                            } else {
                                filterAdapter.updateAll(response.body()?.data!!)
                                for (i in 0..response.body()?.data?.size!!-1) {
                                    shops.add( response.body()?.data?.get(i)!!)
                                }
                            }

                        } else {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                        }
                    }

                })
            }
        }
    }
    fun Filter(){
        Client.getClient()?.create(Service::class.java)?.Filters(lang.appLanguage)
            ?.enqueue(object :Callback<FiltersResponse>{
                override fun onResponse(
                    call: Call<FiltersResponse>,
                    response: Response<FiltersResponse>
                ) {
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                              listAdapter.updateAll(response.body()?.data?.additions_data!!)
                            spaceAdapter.updateAll(response.body()?.data?.space_type!!)
                            space = response.body()?.data?.space_type!![0].id!!
                        }else{

                        }
                    }
                }

                override fun onFailure(call: Call<FiltersResponse>, t: Throwable) {
                    t.printStackTrace()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

            })
    }
}