package com.aait.spacek.UI.Fragments

import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.spacek.Base.BaseFragment
import com.aait.spacek.Listeners.OnItemClickListener
import com.aait.spacek.Models.NotificationModel
import com.aait.spacek.Models.NotificationResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Controllers.NotificationAdapter
import com.aait.spacek.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_notification
    companion object {
        fun newInstance(): NotificationFragment {
            val args = Bundle()
            val fragment = NotificationFragment()
            fragment.setArguments(args)
            return fragment
        }

    }
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager


    lateinit var notificationAdapter: NotificationAdapter
    var notificationModels = ArrayList<NotificationModel>()
    lateinit var title:TextView
    override fun initializeComponents(view: View) {
        title =view.findViewById(R.id.title)
        title.text = getString(R.string.notification)

        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        title.text = getString(R.string.notification)
        linearLayoutManager = LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL,false)
        notificationAdapter = NotificationAdapter(mContext!!,notificationModels,R.layout.recycle_notification)
        //  productAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = notificationAdapter

        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        if(user.loginStatus!!) {
            swipeRefresh!!.setOnRefreshListener {
                getData()

            }
            getData()
        }else{

        }

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
//        subs.clear()
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Notification(lang.appLanguage,user.userData.id!!)?.enqueue(object:
                Callback<NotificationResponse> {
            override fun onFailure(call: Call<NotificationResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                    call: Call<NotificationResponse>,
                    response: Response<NotificationResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {

//
                            notificationAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }



}