package com.aait.spacek.UI.Activities.Appinfo

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Listeners.OnItemClickListener
import com.aait.spacek.Models.BaseResponse
import com.aait.spacek.Models.CallUsResponse
import com.aait.spacek.Models.SocialModel
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Main.MainActivity
import com.aait.spacek.UI.Controllers.SocialAdapter
import com.aait.spacek.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CallUsActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_call_us
    lateinit var back: ImageView
    lateinit var title: TextView
//    lateinit var email: TextView
//    lateinit var phone: TextView
//    lateinit var whats: TextView

   lateinit var social:RecyclerView
    lateinit var socialAdapter: SocialAdapter
    var socialModels = ArrayList<SocialModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var phone:EditText
    lateinit var user_name:EditText
    lateinit var email:EditText
    lateinit var message:EditText
    lateinit var confirm:Button
    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var favourite: LinearLayout
    lateinit var favourite_image: ImageView
    lateinit var favourite_text: TextView

    lateinit var my_reservations: LinearLayout

    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var more: LinearLayout

    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    override fun initializeComponents() {
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        favourite = findViewById(R.id.favourite)
        favourite_image = findViewById(R.id.favourite_image)
        favourite_text = findViewById(R.id.favourite_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)
        home.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","home")
            startActivity(intent)
            finish()}
        favourite.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","notification")
            startActivity(intent)
            finish()
        }
        my_reservations.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","reservation")
            startActivity(intent)
            finish()
        }
        more.setOnClickListener { val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("type","more")
            startActivity(intent)
            finish()}
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        email = findViewById(R.id.email)
        phone = findViewById(R.id.phone)
        user_name = findViewById(R.id.user_name)
        message = findViewById(R.id.message)
        confirm = findViewById(R.id.confirm)
//        whats = findViewById(R.id.whats)

        social = findViewById(R.id.social)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        socialAdapter = SocialAdapter(mContext,socialModels,R.layout.recycle_social)
        socialAdapter.setOnItemClickListener(this)
        social.layoutManager = linearLayoutManager
        social.adapter = socialAdapter
        back.setOnClickListener { onBackPressed()
            finish() }
        title.text = getString(R.string.contact_us)
        getData()
confirm.setOnClickListener {
    if(CommonUtil.checkEditError(user_name,getString(R.string.user_name))||
            CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
            CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
            CommonUtil.checkEditError(email,getString(R.string.email))||
            !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                    CommonUtil.checkEditError(message,getString(R.string.write_mssage))){
        return@setOnClickListener
    }else{
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Contact(lang.appLanguage,user_name.text.toString(),phone.text.toString()
                ,email.text.toString(),message.text.toString())?.enqueue(object :Callback<BaseResponse>{
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        phone.setText("")
                        user_name.setText("")
                        email.setText("")
                        message.setText("")
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

        })
    }

}

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CallUs(lang.appLanguage)?.enqueue(object:
            Callback<CallUsResponse> {
            override fun onFailure(call: Call<CallUsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<CallUsResponse>, response: Response<CallUsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
//                        email.text = response.body()?.data?.email!!
//                        phone.text = response.body()?.data?.phone!!
//                        whats.text = response.body()?.data?.whatsapp!!
                        socialAdapter.updateAll(response.body()?.data?.socials!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (socialModels.get(position).link!!.startsWith("http"))
        {
            Log.e("here", "333")
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(socialModels.get(position).link!!)
            startActivity(i)

        } else {
            Log.e("here", "4444")
            val url = "https://"+socialModels.get(position).link!!
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }

}