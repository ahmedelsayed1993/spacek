package com.aait.spacek.UI.Fragments

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.spacek.Base.BaseFragment
import com.aait.spacek.Gps.GPSTracker
import com.aait.spacek.Gps.GpsTrakerListener
import com.aait.spacek.Listeners.OnItemClickListener
import com.aait.spacek.Models.*
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Main.ReservationDetailsActivity

import com.aait.spacek.UI.Controllers.ReservationAdapter
import com.aait.spacek.Utils.CommonUtil
import com.aait.spacek.Utils.DialogUtil
import com.aait.spacek.Utils.PermissionUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class CurrentFragment:BaseFragment(), OnItemClickListener, GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.app_recycle

    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    lateinit var reservationAdapter: ReservationAdapter
    var favouritesModels = ArrayList<ReservationModel>()
    var orders = ArrayList<ReservationModel>()
    override fun initializeComponents(view: View) {
        rv_recycle =view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL,false)
        reservationAdapter = ReservationAdapter(mContext!!,favouritesModels,R.layout.recycle_reservation)
        reservationAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = reservationAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getLocationWithPermission()

        }
        getLocationWithPermission()

    }

    fun getData(lat:String,lng:String){
        orders.clear()
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        if (user.loginStatus!!) {
            Client.getClient()?.create(Service::class.java)
                ?.Reservations(lang.appLanguage, user.userData.id!!, lat, lng)?.enqueue(object :
                Callback<ReservationResponse> {
                override fun onFailure(call: Call<ReservationResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext!!, t)
                    t.printStackTrace()
                    layNoInternet!!.visibility = View.VISIBLE
                    layNoItem!!.visibility = View.GONE
                    swipeRefresh!!.isRefreshing = false
                }

                override fun onResponse(
                    call: Call<ReservationResponse>,
                    response: Response<ReservationResponse>
                ) {
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                            } else {
                                for (i in 0..response.body()?.data?.size!! - 1) {
                                    if (response.body()?.data?.get(i)?.status!!.equals("pending") || response.body()?.data?.get(
                                            i
                                        )?.status!!.equals("accepted") || response.body()?.data?.get(
                                            i
                                        )?.status!!.equals("inprogress")
                                    ) {
                                        orders.add(response.body()?.data?.get(i)!!)
                                    }
                                }
                                if (orders.isEmpty()) {
                                    layNoItem!!.visibility = View.VISIBLE
                                    layNoInternet!!.visibility = View.GONE
                                    tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                                } else {

//
                                    reservationAdapter.updateAll(orders)
                                }

//

                            }
                        } else {
                            CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                        }
                    }
                }

            })
        }
    }

    override fun onItemClick(view: View, position: Int) {
           val intent = Intent(activity, ReservationDetailsActivity::class.java)
        intent.putExtra("data",favouritesModels.get(position))
        startActivity(intent)

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        }
        else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(activity, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())
                getData(gps.latitude.toString(),gps.longitude.toString())

            }
        }
    }
}