package com.aait.spacek.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.spacek.Base.ParentRecyclerAdapter
import com.aait.spacek.Base.ParentRecyclerViewHolder
import com.aait.spacek.Models.FilterModel

import com.aait.spacek.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class FilterAdapter (context: Context, data: MutableList<FilterModel>, layoutId: Int) :
    ParentRecyclerAdapter<FilterModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        Glide.with(mcontext).load(questionModel.images!!.get(0)).into(viewHolder.image)
        viewHolder.type.text = questionModel.category
        viewHolder.distance.text = questionModel.answer_during.toString()
        viewHolder.address.text = questionModel.desc
        viewHolder.rating.rating = questionModel.rate!!
        viewHolder.closed.text = questionModel.capacity


        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_right)
        animation.setDuration(750)
        viewHolder.itemView.startAnimation(animation)

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var type = itemView.findViewById<TextView>(R.id.category)
        internal var distance = itemView.findViewById<TextView>(R.id.time)
        internal var closed = itemView.findViewById<TextView>(R.id.count)
        internal var address = itemView.findViewById<TextView>(R.id.desc)
        internal var rating = itemView.findViewById<RatingBar>(R.id.rating)



    }
}