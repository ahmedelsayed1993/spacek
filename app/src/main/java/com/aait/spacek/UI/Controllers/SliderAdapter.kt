package com.aait.spacek.UI.Controllers

import android.content.Context
import android.content.Intent
import android.view.MotionEvent
import android.view.ScaleGestureDetector

import android.view.View
import android.widget.ImageView
import com.aait.spacek.R


import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderAdapter




class SliderAdapter (context:Context,list : ArrayList<String>) : CardSliderAdapter<String>(list) {


    var list = list
    var context=context

    lateinit var image:ImageView
    override fun bindView(position: Int, itemContentView: View, item: String?) {
            image = itemContentView.findViewById(R.id.image)
            Glide.with(context).load(item).into(image)
            itemContentView.setOnClickListener {
//                val intent  = Intent(context, ImagesActivity::class.java)
//                intent.putExtra("link",list)
//                context.startActivity(intent)
            }


    }


    override fun getItemContentLayout(position: Int) : Int { return R.layout.card_image_slider }

}