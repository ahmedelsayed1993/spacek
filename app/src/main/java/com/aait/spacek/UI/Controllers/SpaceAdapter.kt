package com.aait.spacek.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import com.aait.spacek.Base.ParentRecyclerAdapter
import com.aait.spacek.Base.ParentRecyclerViewHolder
import com.aait.spacek.Models.ListModel
import com.aait.spacek.R

class SpaceAdapter (context: Context, data: MutableList<ListModel>, layoutId: Int) :
    ParentRecyclerAdapter<ListModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var selected :Int = 0
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.space!!.setText(listModel.name)
        listModel.checked = (selected==position)

        viewHolder.space.setChecked(selected == position)
        viewHolder.space.setTag(position)
        viewHolder.space.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var space=itemView.findViewById<RadioButton>(R.id.space)


    }
}