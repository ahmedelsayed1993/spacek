package com.aait.spacek.UI.Fragments

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.aait.spacek.Base.BaseFragment
import com.aait.spacek.Listeners.OnItemClickListener
import com.aait.spacek.R
import com.aait.spacek.UI.Controllers.SubscribeTapAdapter
import com.google.android.material.tabs.TabLayout

class ReservationFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_orders
    companion object {
        fun newInstance(): ReservationFragment {
            val args = Bundle()
            val fragment = ReservationFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var title:TextView
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: SubscribeTapAdapter? = null
    override fun initializeComponents(view: View) {
        title = view.findViewById(R.id.title)
        title.text = getString(R.string.my_reservations)
        orders = view.findViewById(R.id.orders)
        ordersViewPager = view.findViewById(R.id.ordersViewPager)
        mAdapter = SubscribeTapAdapter(mContext!!,childFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)
    }


}