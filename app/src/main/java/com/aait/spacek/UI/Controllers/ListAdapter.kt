package com.aait.spacek.UI.Controllers

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import com.aait.spacek.Base.ParentRecyclerAdapter
import com.aait.spacek.Base.ParentRecyclerViewHolder
import com.aait.spacek.Models.ListModel
import com.aait.spacek.R


class ListAdapter (context: Context, data: MutableList<ListModel>, layoutId: Int) :
    ParentRecyclerAdapter<ListModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.name!!.setText(listModel.name)
        if (viewHolder.name.isChecked){
            listModel.checked = false
        }else{
            listModel.checked= true
        }
        viewHolder.name.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var name=itemView.findViewById<RadioButton>(R.id.name)


    }
}