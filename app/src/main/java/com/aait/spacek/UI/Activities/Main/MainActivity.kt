package com.aait.spacek.UI.Activities.Main

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Auth.LoginActivity
import com.aait.spacek.UI.Fragments.HomeFragment
import com.aait.spacek.UI.Fragments.MoreFragment
import com.aait.spacek.UI.Fragments.NotificationFragment
import com.aait.spacek.UI.Fragments.ReservationFragment

class MainActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_main
    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var favourite: LinearLayout
    lateinit var favourite_image: ImageView
    lateinit var favourite_text: TextView

    lateinit var my_reservations: LinearLayout

    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var more: LinearLayout

    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    private var fragmentManager: FragmentManager? = null
    var selected = 1
    private var transaction: FragmentTransaction? = null
    internal lateinit var homeFragment: HomeFragment
    internal lateinit var notificationFragment: NotificationFragment
    internal lateinit var reservationFragment: ReservationFragment
    internal lateinit var moreFragment: MoreFragment
    var type = ""
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun initializeComponents() {
        type = intent.getStringExtra("type")!!
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        favourite = findViewById(R.id.favourite)
        favourite_image = findViewById(R.id.favourite_image)
        favourite_text = findViewById(R.id.favourite_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)

        homeFragment = HomeFragment.newInstance()
        notificationFragment = NotificationFragment.newInstance()
        reservationFragment = ReservationFragment.newInstance()
        moreFragment = MoreFragment.newInstance()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container, homeFragment)
        transaction!!.add(R.id.home_fragment_container, notificationFragment)
        transaction!!.add(R.id.home_fragment_container,reservationFragment)
        transaction!!.add(R.id.home_fragment_container,moreFragment)
        transaction!!.commit()

        if (type.equals("home")) {
            showhome()
        }else if (type.equals("notification")){
            if (user.loginStatus!!){
                showreservation()
            } else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }else if (type.equals("reservation")){
            if (user.loginStatus!!){
                showpurchase()
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }else if (type.equals("more")){
            showmore()
        }
        home.setOnClickListener { showhome() }
        favourite.setOnClickListener {
            if (user.loginStatus!!){
                showreservation()
            } else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        my_reservations.setOnClickListener {
            if (user.loginStatus!!){
                showpurchase()
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        more.setOnClickListener { showmore() }

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showhome(){
        selected = 1

        home_image.setImageResource(R.mipmap.homeactive)
        home_text.textColor = Color.parseColor("#ECB832")
        reservation_text.textColor = Color.parseColor("#000000")
        favourite_text.textColor = Color.parseColor("#000000")
        more_text.textColor = Color.parseColor("#000000")
        reservation_image.setImageResource(R.mipmap.order)
        favourite_image.setImageResource(R.mipmap.notification)
        more_image.setImageResource(R.mipmap.more)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, homeFragment)
        transaction!!.commit()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showreservation(){
        selected = 1

        home_image.setImageResource(R.mipmap.homeact)
        favourite_text.textColor = Color.parseColor("#ECB832")
        home_text.textColor = Color.parseColor("#000000")
        reservation_text.textColor = Color.parseColor("#000000")
        more_text.textColor = Color.parseColor("#000000")

        reservation_image.setImageResource(R.mipmap.order)
        favourite_image.setImageResource(R.mipmap.notificatio)
        more_image.setImageResource(R.mipmap.more)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, notificationFragment)
        transaction!!.commit()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showpurchase(){
        selected = 1

        home_image.setImageResource(R.mipmap.homeact)
        reservation_text.textColor = Color.parseColor("#ECB832")
        favourite_text.textColor = Color.parseColor("#000000")
        home_text.textColor = Color.parseColor("#000000")
        more_text.textColor = Color.parseColor("#000000")

        reservation_image.setImageResource(R.mipmap.orde)
        favourite_image.setImageResource(R.mipmap.notification)
        more_image.setImageResource(R.mipmap.more)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, reservationFragment)
        transaction!!.commit()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showmore(){
        selected = 1

        home_image.setImageResource(R.mipmap.homeact)
        more_text.textColor = Color.parseColor("#ECB832")
        reservation_text.textColor = Color.parseColor("#000000")
        favourite_text.textColor = Color.parseColor("#000000")
        home_text.textColor = Color.parseColor("#000000")
        reservation_image.setImageResource(R.mipmap.order)
        favourite_image.setImageResource(R.mipmap.notification)
        more_image.setImageResource(R.mipmap.mo)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, moreFragment)
        transaction!!.commit()
    }

}