package com.aait.spacek.UI.Activities.Main

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Gps.GPSTracker
import com.aait.spacek.Gps.GpsTrakerListener
import com.aait.spacek.Models.FilterModel
import com.aait.spacek.Models.FilterResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.Utils.CommonUtil
import com.aait.spacek.Utils.DialogUtil
import com.aait.spacek.Utils.PermissionUtils
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.lang.Float
import java.util.*
import kotlin.collections.ArrayList

class MapActivity:ParentActivity(),GpsTrakerListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_map
    lateinit var back:ImageView
    lateinit var map:MapView
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    var day = 0
    internal var hmap = HashMap<Marker, FilterModel>()
    internal lateinit var markerOptions: MarkerOptions
    override fun initializeComponents() {
        day = intent.getIntExtra("day",0)
        back  = findViewById(R.id.back)
        map = findViewById(R.id.map)
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        back.setOnClickListener { onBackPressed()
            finish()}

    }

    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!
        googleMap!!.setOnMarkerClickListener(this)
        getLocationWithPermission()
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        if (p0!!.getTitle() != "موقعي") {
            if (hmap.containsKey(p0)) {
                val myShopModel = hmap.get(p0)
                val intent = Intent(this,ProductDetailsActivity::class.java)
                intent.putExtra("id",myShopModel?.id)
                intent.putExtra("day",day)
                startActivity(intent)
            }
        }
        return false
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                getData(gps.getLatitude().toString(),gps.getLongitude().toString())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
    fun getData(lat:String,lng:String){
        // showProgressDialog(getString(R.string.please_wait))

        Client.getClient()?.create(Service::class.java)?.Search(
                lang.appLanguage,
                null,
                lat,
                lng,
                null,
                null,
                null,
                null,
                null,
                null,
                null,null,null
        ,day)?.enqueue(object :
                Callback<FilterResponse> {
            override fun onFailure(call: Call<FilterResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext, t)
                t.printStackTrace()

            }

            override fun onResponse(
                    call: Call<FilterResponse>,
                    response: Response<FilterResponse>
            ) {
                hideProgressDialog()


                if (response.body()?.value.equals("1")) {
                    if (response.body()!!.data?.isEmpty()!!) {

                        googleMap?.clear()

                    } else {
                        for (i in 0..response.body()?.data?.size!!-1){
                            addShop(response.body()?.data?.get(i)!!)
                        }
                    }

                } else {
                    CommonUtil.makeToast(mContext, response.body()?.msg!!)
                }
            }

        })

    }
    internal fun addShop(shopModel: FilterModel) {
        if (shopModel.lat!!.equals("")||shopModel.lng!!.equals("")){

        }else {
            markerOptions = MarkerOptions().position(
                    LatLng(shopModel.lat!!.toDouble(), shopModel.lng!!.toDouble())
            )
                    .title(shopModel.name)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.homelocation))
            myMarker = googleMap!!.addMarker(markerOptions)
            googleMap!!.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                            LatLng(java.lang.Double.parseDouble(shopModel.lat!!), java.lang.Double.parseDouble(shopModel.lng!!)), 8f)
            )
            hmap[myMarker] = shopModel
        }
    }
}