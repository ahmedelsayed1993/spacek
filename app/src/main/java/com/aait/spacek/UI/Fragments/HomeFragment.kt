package com.aait.spacek.UI.Fragments

import android.Manifest
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.spacek.Base.BaseFragment
import com.aait.spacek.Gps.GPSTracker
import com.aait.spacek.Gps.GpsTrakerListener
import com.aait.spacek.Listeners.OnItemClickListener
import com.aait.spacek.Models.FilterModel
import com.aait.spacek.Models.FilterResponse
import com.aait.spacek.Models.ListModel
import com.aait.spacek.Models.ListResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.UI.Activities.Main.ProductDetailsActivity
import com.aait.spacek.UI.Activities.Main.SearchActivity
import com.aait.spacek.UI.Controllers.CatAdapter
import com.aait.spacek.UI.Controllers.HomeAdapter
import com.aait.spacek.UI.Controllers.ListAdapter
import com.aait.spacek.UI.Views.ListDialog
import com.aait.spacek.Utils.CommonUtil
import com.aait.spacek.Utils.DialogUtil
import com.aait.spacek.Utils.PermissionUtils
import com.google.android.gms.maps.model.LatLng
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment:BaseFragment(),OnItemClickListener , GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.fragment_home
    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.setArguments(args)
            return fragment
        }
    }

    lateinit var city:TextView
    lateinit var cat:TextView
    lateinit var search:Button
    lateinit var places:RecyclerView
    lateinit var homeAdapter: HomeAdapter
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var gridLayoutManager1: GridLayoutManager
    lateinit var gridLayoutManager2: GridLayoutManager
    var filterModels = ArrayList<FilterModel>()
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    var rest = 0
    var selected =  0
    var caf = 0
    var ids = ArrayList<Int>()
    var cats = ArrayList<Int>()
    var cities = ArrayList<String>()
    var categories = ArrayList<String>()
    private var mAlertDialog: AlertDialog? = null
    var listModels = ArrayList<ListModel>()
    var listModels1 = ArrayList<ListModel>()
    lateinit var listDialog: ListDialog
    lateinit var listModel: ListModel
    lateinit var listModel1: ListModel
    lateinit var listAdapter: ListAdapter
    lateinit var listAdapter1: CatAdapter
    lateinit var date:TextView
    var day_count = 0
    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents(view: View) {
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy")
        val formatter1 = DateTimeFormatter.ofPattern("EEEE , dd  MMMM")
        val formatter2 = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        var answer: String =  current.format(formatter2)
        var day = LocalDateTime.now().dayOfWeek.toString()
        if (day.contains("MON")){
            day_count = 2
        }else if (day.contains("SUN")){
            day_count = 1
        }
        else if (day.contains("SAT")){
            day_count = 7
        }else if (day.contains("FRI")){
            day_count = 6
        }else if (day.contains("TUE")){
            day_count = 3
        }
        else if (day.contains("WED")){
            day_count = 4
        }else if (day.contains("THR")){
            day_count = 5
        }
        Log.e("dayy",LocalDateTime.now().dayOfWeek.toString())
        city = view.findViewById(R.id.city)
        cat = view.findViewById(R.id.cat)
        search = view.findViewById(R.id.search)
        places = view.findViewById(R.id.places)
        homeAdapter = HomeAdapter(mContext!!,filterModels,R.layout.recycle_home)
        gridLayoutManager = GridLayoutManager(mContext!!,2)
        homeAdapter.setOnItemClickListener(this)
        places.layoutManager = gridLayoutManager
        places.adapter = homeAdapter
        date = view.findViewById(R.id.date)
        date.text = answer
        getLocationWithPermission()
        date.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(mContext!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var m = monthOfYear+1
                date.setText("" + dayOfMonth + "/" + m + "/" + year)
               day_count = c.get(Calendar.DAY_OF_WEEK)
                getLocationWithPermission()
            }, year, month, day)
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            dpd.show()
        }
        city.setOnClickListener {

            val dialog = Dialog(mContext!!)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_list)
            val subs = dialog?.findViewById<RecyclerView>(R.id.subs)
            val confirm = dialog?.findViewById<Button>(R.id.confirm)
            gridLayoutManager1 = GridLayoutManager(mContext, 3)
            listAdapter = ListAdapter(mContext!!,listModels,R.layout.recycler_city)
            listAdapter.setOnItemClickListener(this)
            subs.layoutManager = gridLayoutManager1
            subs.adapter = listAdapter
            getCity()
            confirm.setOnClickListener {
                if (ids.isEmpty()){
                }else{
                    dialog?.dismiss()
                }
            }

            dialog?.show()
        }
        cat.setOnClickListener {
            val dialog = Dialog(mContext!!)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_list)
            val subs = dialog?.findViewById<RecyclerView>(R.id.subs)
            val confirm = dialog?.findViewById<Button>(R.id.confirm)
            gridLayoutManager2 = GridLayoutManager(mContext, 3)
            listAdapter1 = CatAdapter(mContext!!,listModels1,R.layout.recycler_cat)
            listAdapter1.setOnItemClickListener(this)
            subs.layoutManager = gridLayoutManager2
            subs.adapter = listAdapter1
            getCategory()
            confirm.setOnClickListener {
                if (cats.isEmpty()){
                }else{
                    dialog?.dismiss()
                }
            }

            dialog?.show()}
        search.setOnClickListener {
            if (CommonUtil.checkTextError(city,getString(R.string.city))||
                    CommonUtil.checkTextError(cat,getString(R.string.activity))){
                return@setOnClickListener
            }else{
                val intent = Intent(activity,SearchActivity::class.java)
                intent.putExtra("city",ids)
                intent.putExtra("cat",cats)
                intent.putExtra("day",day_count)
                startActivity(intent)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        getLocationWithPermission()
    }
    fun getData(lat:String,lng: String){

        Client.getClient()?.create(Service::class.java)?.Search(lang.appLanguage,null,lat,lng,null,null,null,null,null,null,null,null,null,day_count)
            ?.enqueue(object : Callback<FilterResponse> {
                override fun onResponse(
                    call: Call<FilterResponse>,
                    response: Response<FilterResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                             homeAdapter.updateAll(response.body()?.data!!)
                        }else{
                            CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        }
                    }
                }

                override fun onFailure(call: Call<FilterResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext!!,t)
                    t.printStackTrace()
                }

            })
    }
    fun getCity(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Cities(lang.appLanguage)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listAdapter.updateAll(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                })
    }

    fun getCategory(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Categories(lang.appLanguage)
                ?.enqueue(object : Callback<ListResponse> {
                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                listAdapter1.updateAll(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }

                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name) {
            // listDialog.dismiss()

            listAdapter.notifyDataSetChanged()
            if (listModels.get(position).checked!!) {
                ids.add(listModels.get(position).id!!)
                cities.add(listModels.get(position).name!!)
            } else {
                ids.remove(listModels.get(position).id!!)
                cities.remove(listModels.get(position).name!!)
            }
            city.text = cities.joinToString(separator = ",", postfix = "", prefix = "")
        } else if (view.id==R.id.cat){
                listAdapter1.notifyDataSetChanged()
                if (listModels1.get(position).checked!!){
                    cats.add(listModels1.get(position).id!!)
                    categories.add(listModels1.get(position).name!!)
                }else{
                    cats.remove(listModels1.get(position).id!!)
                     categories.remove(listModels1.get(position).name!!)
                }
                cat.text = categories.joinToString(separator = ",",postfix = "",prefix = "")

        }else{

            val intent = Intent(activity, ProductDetailsActivity::class.java)
            intent.putExtra("id",filterModels.get(position).id)
            intent.putExtra("day",day_count)
            startActivity(intent)
        }

    }
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        }
        else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(activity, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())
                getData(gps.latitude.toString(),gps.longitude.toString())

            }
        }
    }
}