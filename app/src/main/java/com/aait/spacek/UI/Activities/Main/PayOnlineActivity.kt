package com.aait.spacek.UI.Activities.Main

import android.content.Intent
import android.os.Handler
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient




import com.aait.spacek.Base.ParentActivity

import com.aait.spacek.Models.TermsResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PayOnlineActivity : ParentActivity() {

    internal var web: WebView? = null
    internal var id: Int = 0
    internal var user_id = 0

    internal lateinit var pay: String

    protected override val layoutResource: Int
        get() = R.layout.activity_online



    protected override fun initializeComponents() {
        id = intent.getIntExtra("id",0)

        web = findViewById(R.id.web)
        web!!.settings.javaScriptEnabled = true

        web!!.loadUrl("https://spacek.4hoste.com/visa-index/"+id)
        Log.e("url", web!!.url!!)
        web!!.webViewClient = MyWebVew()

        if (web!!.url!!.contains("https://spacek.4hoste.com/payment-success")) {
            startActivity(Intent(this,MainActivity::class.java))
            finish()

        }else if (web!!.url!!.contains("https://spacek.4hoste.com/payment-fail")){

            onBackPressed()
            finish()
           // CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
        }
    }

    protected fun hideInputType(): Boolean {
        return false
    }

    inner class MyWebVew : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, request: String): Boolean {
            Log.e("link",request)
            if (request.contains("https://spacek.4hoste.com/payment-fail")) {
                view.loadUrl(request)
                Handler().postDelayed({
                    Handler().postDelayed({

                        //onBackPressed()
                        finish()
                      //  CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
                    }, 3000)
                }, 3000)

            } else if (request.contains("https://spacek.4hoste.com/payment-success")) {


                startActivity(Intent(this@PayOnlineActivity,MainActivity::class.java))
                finish()

            }else{
                view.loadUrl(request)
                Handler().postDelayed({
                    Handler().postDelayed({

                        //onBackPressed()
                        finish()
                        //CommonUtil.makeToast(mContext,getString(R.string.wrong_pay))
                    }, 3000)
                }, 3000)

            }
            return true
        }
    }


}
