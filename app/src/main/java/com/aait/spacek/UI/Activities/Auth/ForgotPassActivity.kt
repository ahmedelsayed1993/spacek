package com.aait.spacek.UI.Activities.Auth

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Models.UserResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPassActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_forgot_pass

    lateinit var phone: EditText
    lateinit var confirm: Button

    override fun initializeComponents() {
        phone = findViewById(R.id.phone)
        confirm = findViewById(R.id.confirm)
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))){
                return@setOnClickListener
            }else{

                ForgotPass()
            }
        }

    }
    fun ForgotPass(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Forgot(lang.appLanguage,phone.text.toString())?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        val intent =
                            Intent(this@ForgotPassActivity, NewPassActivity::class.java)
                        intent.putExtra("data", response.body()?.data)
                        startActivity(intent)
                        finish()

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })

    }

}