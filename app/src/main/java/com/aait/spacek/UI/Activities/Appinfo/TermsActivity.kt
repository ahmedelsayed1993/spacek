package com.aait.spacek.UI.Activities.Appinfo

import android.widget.ImageView
import android.widget.TextView
import com.aait.spacek.Base.ParentActivity
import com.aait.spacek.Models.TermsResponse
import com.aait.spacek.Network.Client
import com.aait.spacek.Network.Service
import com.aait.spacek.R
import com.aait.spacek.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TermsActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_terms
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var about: TextView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        about = findViewById(R.id.about)
        back.setOnClickListener { onBackPressed()
            finish() }
        title.text = getString(R.string.terms_conditions)
        getData()


    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Terms(lang.appLanguage)?.enqueue(object:
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        about.text = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}