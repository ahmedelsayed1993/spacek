package com.aait.spacek.Listeners

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View, position: Int)
}
