package com.aait.spacek.Models

import java.io.Serializable

class ReservationResponse:BaseResponse(),Serializable {
    var data:ArrayList<ReservationModel>?=null
}