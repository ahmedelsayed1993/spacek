package com.aait.spacek.Models

import java.io.Serializable

class SocialModel:Serializable {
    var name:String?=null
    var link:String?=null
    var logo:String?=null
}