package com.aait.spacek.Models

import java.io.Serializable

open class BaseResponse : Serializable {
    var key:String?=null
    var value:String?=null
    var msg:String?=null
    var code:String?=null
    var user_status:String?=null
}