package com.aait.spacek.Models

import java.io.Serializable

data class Response(
	val date: String? = null,
	val image: String? = null,
	val address: String? = null,
	val categoryName: String? = null,
	val distance: Int? = null,
	val lng: String? = null,
	val numbers: Int? = null,
	val foodType: String? = null,
	val reservationDayPrice: String? = null,
	val total: String? = null,
	val categoryId: Int? = null,
	val rate: Int? = null,
	val provider: List<ProviderItem?>? = null,
	val name: String? = null,
	val answerDuring: String? = null,
	val id: Int? = null,
	val time: String? = null,
	val isPaid: String? = null,
	val reservationPrice: String? = null,
	val lat: String? = null,
	val status: String? = null,
	val desc: String? = null
)

 class ProviderItem:Serializable {
	 val hour_price: Int? = null
	 val images: List<String?>? = null
	 val address: String? = null
	 val distance: Int? = null
	 val lng: String? = null
	 val is_open: Boolean? = null
	 val food_type: String? = null
	 val type: Int? = null
	 val capacity: String? = null
	 val hour: Any? = null
	 val rate: Int? = null
	 val user_id: Int? = null
	 val rate_price: Int? = null
	 val review: Int? = null
	 val booking_link: String? = null
	 val name: String? = null
	 val answer_during: String? = null
	 val id: Int? = null
	 val category: String? = null
	 val reservation_price: String? = null
	 val lat: String? = null
	 val desc: String? = null
 }


