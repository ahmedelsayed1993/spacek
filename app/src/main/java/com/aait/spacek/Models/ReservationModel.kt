package com.aait.spacek.Models

import java.io.Serializable

class ReservationModel:Serializable {
    val date: String? = null
    val image: String? = null
    val address: String? = null
    val category_name: String? = null
    val distance: Int? = null
    val lng: String? = null
    val numbers: Int? = null
    val food_type: String? = null
    val reservation_day_price: String? = null
    val total: String? = null
    val category_id: Int? = null
    val rate: Int? = null
    val provider: ArrayList<ProviderItem?>? = null
    val name: String? = null
    val answer_during: String? = null
    val id: Int? = null
    val time: String? = null
    val is_paid: String? = null
    val reservation_price: String? = null
    val lat: String? = null
    val status: String? = null
    val desc: String? = null
}