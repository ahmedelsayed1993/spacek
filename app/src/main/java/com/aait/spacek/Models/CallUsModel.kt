package com.aait.spacek.Models

import java.io.Serializable

class CallUsModel:Serializable {
    var email:String?=null
    var phone:String?=null
    var whatsapp:String?=null
    var socials:ArrayList<SocialModel>?=null
}