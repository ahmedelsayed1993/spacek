package com.aait.spacek.Models

import java.io.Serializable

class ReservationDetailsModel:Serializable {
    var name:String?=null
    var phone:String?=null
    var email:String?=null
    var notes:String?=null
    var date:String?=null
    var time:String?=null
    var count:Int?=null
    var status:String?=null
    var is_paid:Boolean?=null
}