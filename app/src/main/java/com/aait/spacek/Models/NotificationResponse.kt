package com.aait.spacek.Models

import java.io.Serializable

class NotificationResponse:BaseResponse(),Serializable {
    var data:ArrayList<NotificationModel>?=null
}