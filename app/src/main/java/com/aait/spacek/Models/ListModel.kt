package com.aait.spacek.Models

import java.io.Serializable

class ListModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var checked:Boolean?=null
}