package com.aait.spacek.Models

import java.io.Serializable

class FilterModel:Serializable {
    var id:Int?=null
    var images:ArrayList<String>?=null
    var name:String?=null
    var desc:String?=null
    var food_type:String?=null
    var distance:Float?=null
    var rate:Float?=null
    var rate_price:Float?=null
    var address:String?=null
    var category:String?=null
    var user_id:Int?=null
    var review:Int?=null
    var is_open:Boolean?=null
    var type:Int?=null
    var booking_link:String?=null
    var lat:String?="24.66666"
    var lng:String?="25.22145"
    var capacity:String?=null
    var answer_during:String?=null
}