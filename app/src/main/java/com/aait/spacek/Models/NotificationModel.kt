package com.aait.spacek.Models

import java.io.Serializable

class NotificationModel:Serializable {
    var image:String?=null
    var msg:String?=null
    var type:String?=null
    var reservation:String?=null
}